# DateMatchers - IAW - 2020

## Backend

El backend se genero basada en [LoopBack 4 CLI](https://loopback.io/doc/en/lb4/Command-line-interface.html). Para mas información del framework
[accede a la documentación](https://loopback.io/doc/en/lb4/index.html).

## Frontend

El frontend se genero basada en [Angular 10](https://angular.io/). Para la parte estetica se utilizo  [Material Angular](https://material.angular.io/)
para mas informacion del la version del framework [accede a la documentación](https://v10.angular.io/docs).


## Levantar la aplicación
para el entorno de desarrollo se realizo un `docker-compose.yml` en el cual se integra el entorno listo para trabajar con el frontend y el backend, como base de datos usa [MongoDB Cloud](https://www.mongodb.com/cloud)
para levantar a aplicación ejecute

```sh
docker-compose up #agregar -d para levantar la aplicación en segundo plano
```

## acceder a la aplicación

**Backend**: http://localhost:3000

**Frontend**: http://localhost:4200


## accesos
```json
{
  "email": "peron@gmail.com",
  "password": "PeronFijo45"
},{
  "email": "milton.sosa.22@gmail.com",
  "password": "peronfijo1"
}
```

