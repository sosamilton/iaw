import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  Option,
  Votes,
} from '../models';
import {authenticate} from '@loopback/authentication';

import {OptionRepository} from '../repositories';

export class OptionVotesController {
  constructor(
    @repository(OptionRepository) protected optionRepository: OptionRepository,
  ) { }

  @get('/options/{id}/votes', {
    responses: {
      '200': {
        description: 'Array of Option has many Votes',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Votes)},
          },
        },
      },
    },
  })
  async find(
    @param.path.string('id') id: string,
    @param.query.object('filter') filter?: Filter<Votes>,
  ): Promise<Votes[]> {
    return this.optionRepository.votes(id).find(filter);
  }

  @post('/options/{id}/votes', {
    responses: {
      '200': {
        description: 'Option model instance',
        content: {'application/json': {schema: getModelSchemaRef(Votes)}},
      },
    },
  })
  async create(
    @param.path.string('id') id: typeof Option.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Votes, {
            title: 'NewVotesInOption',
            exclude: ['id'],
            optional: ['optionId']
          }),
        },
      },
    }) votes: Omit<Votes, 'id'>,
  ): Promise<Votes> {
    return this.optionRepository.votes(id).create(votes);
  }
  @authenticate('jwt')
  @patch('/options/{id}/votes', {
    responses: {
      '200': {
        description: 'Option.Votes PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Votes, {partial: true}),
        },
      },
    })
    votes: Partial<Votes>,
    @param.query.object('where', getWhereSchemaFor(Votes)) where?: Where<Votes>,
  ): Promise<Count> {
    return this.optionRepository.votes(id).patch(votes, where);
  }
  @authenticate('jwt')
  @del('/options/{id}/votes', {
    responses: {
      '200': {
        description: 'Option.Votes DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.string('id') id: string,
    @param.query.object('where', getWhereSchemaFor(Votes)) where?: Where<Votes>,
  ): Promise<Count> {
    return this.optionRepository.votes(id).delete(where);
  }
}
