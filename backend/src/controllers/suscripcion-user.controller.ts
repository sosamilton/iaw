import {
  repository,
} from '@loopback/repository';
import {
  param,
  get,
  getModelSchemaRef,
} from '@loopback/rest';
import {
  Suscripcion,
  User,
} from '../models';
import {SuscripcionRepository} from '../repositories';

export class SuscripcionUserController {
  constructor(
    @repository(SuscripcionRepository)
    public suscripcionRepository: SuscripcionRepository,
  ) { }

  @get('/suscripcions/{id}/user', {
    responses: {
      '200': {
        description: 'User belonging to Suscripcion',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(User)},
          },
        },
      },
    },
  })
  async getUser(
    @param.path.string('id') id: typeof Suscripcion.prototype.id,
  ): Promise<User> {
    return this.suscripcionRepository.user(id);
  }
}
