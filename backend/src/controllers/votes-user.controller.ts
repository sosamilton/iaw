import {
  repository,
} from '@loopback/repository';
import {
  param,
  get,
  getModelSchemaRef,
} from '@loopback/rest';
import {
  Votes,
  User,
} from '../models';
import {VotesRepository} from '../repositories';

export class VotesUserController {
  constructor(
    @repository(VotesRepository)
    public votesRepository: VotesRepository,
  ) { }

  @get('/votes/{id}/user', {
    responses: {
      '200': {
        description: 'User belonging to Votes',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(User)},
          },
        },
      },
    },
  })
  async getUser(
    @param.path.string('id') id: typeof Votes.prototype.id,
  ): Promise<User> {
    return this.votesRepository.user(id);
  }
}
