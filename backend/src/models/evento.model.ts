import {Entity, hasMany, model, property, belongsTo} from '@loopback/repository';
import {Option} from './option.model';
import {User} from './user.model';

@model({settings: {strict: false}})
export class Evento extends Entity {
  @property({
    type: 'string',
    required: true,
  })
  titulo: string;

  @property({
    type: 'string',
    required: true,
  })
  email: string;

  // @property({
  //   type: 'string',
  // })
  // userId: string;

  @property({
    type: 'string',
    required: true,
  })
  password: string;

  @property({
    type: 'date',
    required: true,
  })
  desde: string;


  @property({
    type: 'date',
    required: true,
  })
  hasta: string;

  @belongsTo(() => User)
  userId: string;
  
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'string',
  })
  descripcion?: string;

  @property({
    type: 'string',
  })
  duracion?: string;

  @hasMany(() => Option)
  options: Option[];
  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Evento>) {
    super(data);
  }
}

export interface EventoRelations {
  // describe navigational properties here
}

export type EventoWithRelations = Evento & EventoRelations;
