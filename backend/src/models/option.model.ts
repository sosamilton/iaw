import {Entity, model, property, hasMany} from '@loopback/repository';
import {Votes} from './votes.model';

@model({settings: {strict: false}})

export class Option extends Entity {
  @property({
    type: 'number',
    required: true,
  })
  dia: number;

  @property({
    type: 'string',
    required: true,
  })
  hora: string;

  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'string',
  })
  eventoId?: string;

  @hasMany(() => Votes)
  votes: Votes[];
  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Option>) {
    super(data);
  }
}

export interface OptionRelations {
  // describe navigational properties here
}

export type OptionWithRelations = Option & OptionRelations;
