import {Entity, model, property, belongsTo} from '@loopback/repository';
import {User} from './user.model';

@model({settings: {strict: false}})
export class Suscripcion extends Entity {
  @property({
    type: 'string',
    required: true,
  })
  preapproval_plan_id: string;

  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'number',
    required: true,
  })
  payer_id: number;

  @property({
    type: 'string',
    required: true,
  })
  status: string;

  @property({
    type: 'date',
    required: true,
  })
  date_created: string;

  @belongsTo(() => User)
  userId: string;
  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Suscripcion>) {
    super(data);
  }
}

export interface SuscripcionRelations {
  // describe navigational properties here
}

export type SuscripcionWithRelations = Suscripcion & SuscripcionRelations;
