import {Entity, hasMany, hasOne, model, property} from '@loopback/repository';
import { UserCredentials } from '@loopback/authentication-jwt';
import {Evento} from './evento.model';

@model({settings: {strict: false}})
export class User extends Entity {
    @property({
        type: 'string',
        id: true,
        generated: true,
    })
    id?: string;

    @property({
        type: 'string',
    })
    realm?: string;

    @property({
        type: 'string',
        required: true,
    })
    email: string;

    @property({
        type: 'string',
    })
    rol: string;

    @property({
        type: 'boolean',
    })
    emailVerified?: boolean;
    
    @property({
        type: 'string',
    })
    verificationToken?: string;
    
    @hasOne(() => UserCredentials)
    userCredentials: UserCredentials;

    @hasMany(() => Evento)
    eventos: Evento[];    

    [prop: string]: any;

    constructor(data?: Partial<User>){
        super(data);
    }}

export interface UserRelations {
}

export type UserWithRelations = User & UserRelations;

