import { Component } from '@angular/core';
import { TokenService } from './services/token.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'DateMatchers';

  constructor(
    public router: Router,
    public tokenService: TokenService
  ) {
  }

  public logout(): void {
    this.tokenService.signOut();
    // Go back to the home route
    this.router.navigate(['/']);
  }

  public authenticated(): boolean {
    return this.tokenService.isAuthenticated();
  }

  public isAdmin(): boolean {
    // return this.tokenService.isAdmin();
    return false;
  }


}
