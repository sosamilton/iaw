import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { environment } from './../environments/environment';
import { ApiModule, Configuration } from './api';
import { TokenService } from './services/token.service'
import { AppComponent } from './app.component';
import { MaterialModule } from './material.module'; // material module imported
import { EventoComponent } from './components/evento/evento.component';
import { CrearEventoComponent } from './components/evento/add-evento.component';
import { EditEventoComponent } from './components/evento/edit-evento.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { InicioComponent } from './components/inicio/inicio.component';
import { LogInComponent } from './components/log-in/log-in.component';
import { RegisterComponent } from './components/register/register.component';
import { OpcionesComponent } from './components/opciones/opciones.component';
import { VotacionComponent } from './components/votacion/votacion.component';
import { PreVotacionComponent } from './components/votacion/pre-votacion.component';
import { GraciasComponent } from './components/gracias/gracias.component';
import { MercadoPagoButtonComponent } from './components/mercadopago-button/mercadopago-button.component'
import * as firebase from 'firebase/app';
import 'firebase/functions';

firebase.default.initializeApp(environment.firebaseConfig);

@NgModule({
  declarations: [
    AppComponent,
    EventoComponent,
    CrearEventoComponent,
    DashboardComponent,
    EditEventoComponent,
    PageNotFoundComponent,
    InicioComponent,
    LogInComponent,
    RegisterComponent,
    OpcionesComponent,
    MercadoPagoButtonComponent,
    VotacionComponent,
    PreVotacionComponent,
    GraciasComponent,
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    ApiModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    MaterialModule,
    FlexLayoutModule,
    ReactiveFormsModule,
  ],
  providers: [
    {
      provide: Configuration,
      useFactory: (authService: TokenService) => new Configuration(
        {
          basePath: environment.apiUrl,
          accessToken: authService.getToken.bind(authService)
        }
      ),
      deps: [TokenService],
      multi: false
    },
    TokenService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
