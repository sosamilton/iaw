import { Component, OnInit } from '@angular/core';
import { EventoService } from '../../services/evento.service';
import { Evento } from '../../api/model/evento';
// import { Evento } from '../api/model/evento';
// import { EventoControllerService } from '../api';
// import { MensajeService } from '../services/mensaje.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: [ './dashboard.component.css' ]
})

export class DashboardComponent implements OnInit {
  // public eventos: Evento[];
  

  constructor(
    public eventoService: EventoService
  ){ 
    this.eventoService.updateEventos();
    // this.eventoService.eventos.subscribe(eventos => {
      // this.eventos = eventos;
    // });
  }
  
  displayedColumns = ['titulo', 'desde', 'hasta', 'description', 'delete'];
  
  ngOnInit():void{
    // this.eventoService.updateEventos();
  }
  
  delete(id: string): void {
    const idDate = id;
    this.eventoService.deleteEvento(id);
  }
  
}