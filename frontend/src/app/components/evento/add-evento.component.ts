import { Component, OnInit, Input } from '@angular/core';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EventoService } from '../../services/evento.service';
import { Evento } from '../../api/model/evento'
import { UserService } from '../../services/user.service';
import { User } from 'src/app/api';

@Component({
  selector: 'app-add-evento',
  templateUrl: './add-evento.component.html',
  styleUrls: ['./evento.component.css']
})

export class CrearEventoComponent implements OnInit {
  form: FormGroup;
  evento: Evento;
  user: User;

  constructor(
    private eventoService: EventoService,
    private location: Location,
    public userService:UserService, 
    private formBuilder: FormBuilder
    ) {
  }
  
  ngOnInit() {
    if (this.userService.authenticated()) {
      this.userService.user.subscribe(user => {
        this.user = user;
      });
    }
    this.form = this.formBuilder.group({
        titulo: ['', Validators.required],
        password: ['', Validators.required],
        descripcion: ['', Validators.required],
        desde: [new Date(), Validators.required],
        duracion: ['', Validators.required],
        email: [this.user?this.user.email:"", [Validators.required, Validators.email]],
        hasta: [new Date(), Validators.required],
        userId: [this.user?this.user.id:''],
    });
  }
  
  onSubmit() {
    if (this.form.invalid) {
        return;
    }
    this.evento = this.form.value;
    
    if (this.userService.authenticated()) {
      this.evento.userId =  this.user.id;
      this.evento.email = this.user.email;
    }

    this.eventoService.createEvento(this.evento);
  }

  onReset() {
      this.form.reset();
  }

  goBack(): void {
    this.location.back();
  }
    
}
