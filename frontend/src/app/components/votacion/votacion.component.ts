import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Option } from '../../api/model/option';
import { EventoService } from '../../services/evento.service'
import { Evento } from '../../api/model/evento'
import { OpcionService } from 'src/app/services/opcion.service';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-votacion',
  templateUrl: './votacion.component.html',
  styleUrls: ['./votacion.component.css']
})
export class VotacionComponent implements OnInit {
 
    evento: Evento;
    opciones: Option[];
    form: FormGroup;
    eventoId: string;
    userId: string;
    listOpciones: Option[];

    dias: {[key: number]: string} = {
      0:'Lunes',
      1: 'Martes',
      2: 'Miercoles',
      3: 'Jueves',
      4: 'Viernes',
      5: 'Sabado',
      6: 'Domingo'
    };
    
    constructor(
      private route: ActivatedRoute,
      private eventoService: EventoService,
      public userService:UserService, 
      private opcionService:OpcionService, 
      private formBuilder: FormBuilder,
      private location: Location
      ) {
        this.eventoService.updateEventos();
      }
      
      ngOnInit(): void {
        this.eventoId = this.route.snapshot.paramMap.get('id');
        this.opcionService.updateDataOpcionesFromEvento(this.eventoId);
        this.eventoService.selectEvento(this.eventoId);

        this.eventoService.getEvento().subscribe(evento => {
          this.evento = evento;
        });

        this.form = this.formBuilder.group({
          user: [null, Validators.required],
          votos: this.formBuilder.array([])
        });
        
        this.userId = '';
        if (this.userService.authenticated()) {
          this.userId = this.userService.getUserId();
          this.form.controls['user'].setValue(this.userId);
        }else{
          // this.form.controls['userId'].setValue(this.userId);
        }

        this.opcionService.options.subscribe(opciones => {
          this.opciones = opciones;
          this.listOpciones = opciones;
          if (this.listOpciones) {
            opciones.forEach( (option: Option) => {
              this.getFormVotos().push(this.createOpcionFormGroup(option))
            });
          }
        });
        
      }

      getFormVotos(){
        return this.form.get('votos') as FormArray
      }
      
      public votos(): FormArray{
        return this.form.get('votos') as FormArray
      }
    
      getOpciones(){
        return this.listOpciones;
        // // this.opcionService.updateDataOpcionesFromEvento(id);
      }
          
      createOpcionFormGroup(data?: Option): FormGroup {
        return this.formBuilder.group({
          optionId: [data?.id, Validators.required],
          userId: [this.userId?this.userId:this.form.value.user, Validators.required],
          valor: [null,Validators.required],
        });
      }

      onSubmit(){
        const votos = [];
        this.form.value.votos.forEach(voto => {
          voto.userId = this.form.value.user;
          votos.push(voto);
        });
        this.form.controls['votos'].setValue(votos);
        if (this.form.invalid) {         
          return false;
        }
        this.opcionService.createVotesListFromOption(this.form.value.votos);
      }


      goBack(): void {
        this.location.back();
      }
      
  }