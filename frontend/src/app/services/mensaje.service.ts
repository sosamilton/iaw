import { Injectable, NgZone } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root',
})
export class MensajeService {

  constructor(
    public snackBar: MatSnackBar,
    private zone: NgZone
  ) {}

  text:string;

  public open(text, action = 'success', duration = 4000) {
    this.zone.run(() => {
      this.snackBar.open(text, action, { duration });
    });
  }

}