import { Injectable } from '@angular/core';
import { MensajeService } from './mensaje.service';
import { Option } from '../api/model/option';
import { Votes } from '../api/model/votes';
import { OptionControllerService, VotesControllerService, EventoOptionControllerService, OptionVotesControllerService } from '../api';
import { EventoService } from './evento.service';
import { Evento } from '../api/model/evento';
import { Router } from '@angular/router';
import { Observable, BehaviorSubject } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { HttpUrlEncodingCodec } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class OpcionService {
  optionProvider = new BehaviorSubject<Option>(null);
  option = this.optionProvider.asObservable();
  optionsProvider = new BehaviorSubject<Option[]>([]);
  options = this.optionsProvider.asObservable();
  evento: Evento;
  codec = new HttpUrlEncodingCodec;

  constructor(
    private mensajeService: MensajeService,
    private opcionService: OptionControllerService,
    private opcionVoteService: OptionVotesControllerService,
    private eventoService: EventoService,
    private eventoOpcionService: EventoOptionControllerService,
    private router: Router
  ) { 
  }

  ngOnInit() {
    this.eventoService.getEvento().subscribe(evento => {
      this.evento = evento;
      this.updateDataOpcionesFromEvento(this.evento);
    });
  }
  
  createOptionListFromEvent(opcionList: Option[]): void {
    var id;
    opcionList.forEach( (option: Option) => {
      id = option.eventoId;
      this.eventoOpcionService.eventoOptionControllerCreate(option.eventoId, option)
      .subscribe((opcion) => {
        // const newArr = [...opciones, opcion];
        // this.optionsProvider.next(newArr);
      });
    });
    // this.updateDataOpcionesFromEvento(id);
    this.mensajeService.open('el listado de opcion se cargo correctamente');
    this.router.navigate(['/evento/'+id]);
  }

  createVotesListFromOption(votesList: Votes[]): void {
    votesList.forEach( (vote: Votes) => {
      vote.valor= +vote.valor;
      this.opcionVoteService.optionVotesControllerCreate(vote.optionId, vote)
      .subscribe((opcion) => {
        this.mensajeService.open('los votos se cargaron correctamente');
        this.router.navigate(['/gracias']);
      });
    });
  }


  updateDataOpcionesFromEvento(id): void{
    this.optionsProvider.next(null);
    let filtro = "{\"include\": [{\"relation\":\"votes\"}]}";
    this.eventoOpcionService.eventoOptionControllerFind(id, {"filter":[filtro]}).subscribe((opciones) => {
        this.optionsProvider.next(opciones);
    })
  }
  
  updateOpcionesFromEvento(list: Option[] ): void {
    list.forEach( (option: Option) => {
      if (!option.id) {
        this.eventoOpcionService.eventoOptionControllerCreate(option.eventoId, option)
        .subscribe((opcion) => {
        });
      }else{
        this.opcionService.optionControllerUpdateById(option.id, option)
        .subscribe((opcion) => {
        });
      }
    });
    this.mensajeService.open('Se actualizo correctamente la lista de opciones del evento');
  }


  deleteOption(id: string): void {
    const idDate = id;
    this.opcionService.optionControllerDeleteById(idDate)
    .subscribe((data: any)=>{
      // this.options.pipe(
      //   map(opciones => opciones.filter(opcion => opcion.id != id))
      // ).subscribe(events => {
      //   this.optionsProvider.next(events);
      // }) 
      this.mensajeService.open('el opcion #'+idDate+' se elimino correctamente');
    });
  }

  createOption(opcion): void {
    this.opcionService.optionControllerCreate(opcion)
    .subscribe((opcion) => {
      this.options.pipe(take(1)).subscribe(opciones => {
        const newArr = [...opciones, opcion];
        this.optionsProvider.next(newArr);
      })
      this.mensajeService.open('el opcion #'+opcion.id+' se creo correctamente');
      this.router.navigate(['/opcion/'+opcion.id+'/opciones']);
    });
  }  


  getOption():Observable<Option>{
      return this.option;
  }


  getOpciones(): Observable<Option[]> {
    return this.options;
  }

  getVotes(){
    const response = [{1:{name:"si",cant:0 }},{2:{name:"no se",cant:0 }},{3:{name:"no",cant:0 }}];
    response.forEach(responese => {
      // let cant = this.options.pipe(map(opciones => opciones.find(e => e.id === id)));
    });
    // let opcionFilter = this.options.pipe(map(opciones => opciones.find(e => e.id === id)));
    // opcionFilter.subscribe(event => {
    //   this.optionProvider.next(event)
    // });
  }
  
  selectOption(id):Observable<Option>{
    let opcionFilter = this.options.pipe(map(opciones => opciones.find(e => e.id === id)));
    opcionFilter.subscribe(event => {
      this.optionProvider.next(event)
    });
    return this.getOption();
  }


}
