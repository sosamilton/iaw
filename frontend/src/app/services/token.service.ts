import { Injectable } from '@angular/core';
import { User } from '../api/model/user'
const TOKEN_KEY = 'auth-token';
const USER_KEY = 'auth-user';
const USER_DATA = 'user';

@Injectable({
  providedIn: 'root'
})

export class TokenService {
  accessToken: string;
  user: User = null;
  constructor() {}

  signOut(): void{
    sessionStorage.clear();
    this.accessToken = null;
  }
  
  saveToken(token: string): void{
    this.accessToken = token;
    sessionStorage.removeItem(TOKEN_KEY);
    sessionStorage.setItem(TOKEN_KEY, token);
  }

  saveUser(user): void {
    sessionStorage.removeItem(USER_KEY);
    sessionStorage.setItem(USER_KEY, JSON.stringify(user.id));
    sessionStorage.setItem(USER_DATA, JSON.stringify(user));
  }

  getUser(): User {
    // let data= sessionStorage.getItem(USER_DATA);
    return JSON.parse(sessionStorage.getItem(USER_DATA));
    // user.email
  }
  
  getToken():string {
    return sessionStorage.getItem(TOKEN_KEY);
  }

  isAuthenticated():boolean {
    if (this.getToken()){
      return true;
    }
    return false;
  }

  isAdmin():boolean {
    if (this.user.rol == 'admin'){
      return true;
    }
    return false;
  }

}
