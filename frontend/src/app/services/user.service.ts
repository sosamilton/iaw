import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { EventoService } from './evento.service';
import { TokenService } from './token.service';
import { UserControllerService, User } from '../api';
import { MensajeService } from './mensaje.service';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class UserService {
  accessToken: string;
  public userProvider = new BehaviorSubject<User>(null);
  public user = this.userProvider.asObservable();
  public usersProvider = new BehaviorSubject<User[]>(null);
  public users = this.usersProvider.asObservable();
  
  constructor(
    private userService:UserControllerService, 
    private router:Router,
    private mensajeService: MensajeService,
    private tokenService: TokenService
  ) {
    this.updateUser();
  }

  ngOnInit() {
    // this.updateUser();
  }

  logout(): void {
    this.tokenService.signOut();
    this.router.navigate(['/']);
  }

  getToken():string{
    return this.tokenService.getToken();
  }

  login(user){
    this.userService.userControllerLogin(user)
      .subscribe((resp)=>{
        this.tokenService.saveToken(resp.token);
        this.updateUser();
      }
    )
  }

  getUser(): Observable<User>{
    return this.user;
  }

  updateUser():void{
    let rutas: string[] = ['/inicio', '/login', '/',  '/gracias', '/register']; 
    if (this.tokenService.isAuthenticated()){
      if (this.tokenService.getUser()) {
        this.userProvider.next(this.tokenService.getUser());
        if (rutas.includes(this.router.url)) {
          this.router.navigateByUrl("/dashboard");
        }
      }else{
        this.userService.userControllerWhoAmI()
        .subscribe((user)=>{
            this.tokenService.saveUser(user);
            this.userProvider.next(user);
            if (rutas.includes(this.router.url)) {
              this.router.navigateByUrl("/dashboard");
            }
          }
        )
      }
    }
  }

  updateUsers():void{
    if (this.tokenService.isAuthenticated()){
      this.userService.userControllerWhoAmI()
      .subscribe((user)=>{
          if (user.rol == "admin"){
            this.userService.userControllerUsers().subscribe(users => {
              this.usersProvider.next(users);
            })
          }
        }
      )
    }
  }

  authenticated(): boolean {
    return this.tokenService.isAuthenticated();
  }

  signUp(user): void {
    this.userService.userControllerSignUp(user)
    .subscribe((resp)=>{
        this.mensajeService.open('el usuario #'+resp.realm+' se creo correctamente');
        this.router.navigateByUrl("/login")
      }
    )
  }

  getUserId(): string {
    return this.tokenService.getUser().id
  }

  getUserEmail(): string {
    return  this.tokenService.getUser().email
  }

  
}