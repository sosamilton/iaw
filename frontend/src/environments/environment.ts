// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiUrl: 'http://localhost:3000',
  firebaseConfig:{
    apiKey: "AIzaSyDmG89CqXXbIJiA1rkFz3r8SNhlW-PhlSY",
    authDomain: "datematchers-iaw.firebaseapp.com",
    projectId: "datematchers-iaw",
    storageBucket: "datematchers-iaw.appspot.com",
    messagingSenderId: "45350095167",
    appId: "1:45350095167:web:0078184fa4e302a4deeb8a",
    measurementId: "G-Z98YJQMRHN"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
